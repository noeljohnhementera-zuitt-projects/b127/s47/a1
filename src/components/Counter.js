
// s47 Discussion

import { useState, useEffect, Fragment } from 'react';
import { Button } from 'react-bootstrap';
// What does use effect do?
	// By using this hook, you tell react that you're component or elements needs to do
		// something after render. React will remember the function you passed (also known as 'side-effect')
		// and call it after performing the DOM updates

// Syntax of useEffect
/*
	useEffect(()=> {}. [])
		- kailangan may empty [ ] para hindi infinite loop

useEffect allows us to perform tasks/function on initial render:
	- when the component is displayed for the first time

What allows us to control when our useEffect will run AFTER the initial render?
	- we add an optional dependency ARRAY to control when useEffect will run, instead that it
	runs on initial render AND when states are updated, we can control the useEffect to run only when
	the state/s in the dependency array is updated.	
*/

export default function Counter () {
	const [count, setCount] = useState(0)

	useEffect(() =>{
		document.title = `You clicked ${count} times` 
	}, [count])
	// This will add a side-effect on the Meta Title

	return (
		<Fragment>
			<p>You clicked {count} times!</p>
			<Button variant="primary" 
				onClick={() => setCount (count + 1)} > 
			Click me!</Button>
		</Fragment>
	)
}