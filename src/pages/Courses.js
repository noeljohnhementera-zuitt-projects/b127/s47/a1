import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';
import { Fragment } from 'react';

export default function Courses (){
	// Checks to see if the mock data was captured
	console.log(courseData)
	console.log(courseData[0])

	// for us to be able to display the courses from the data file, we are going to use the map()
	// The "map" method loops through the individual course objects in our array and returns a component for each course
	// Multiple components created through the map method must have a UNIQUE KEY that will help Reactjs identify which elements
		// have been changed, added or removed
	// Laging may kasama na key={course.id} = to keep track of the data of courses
	
	// Props kinuha sa CourseCard
	const courses = courseData.map(course =>{ // course is a props
		return (

			< CourseCard key = {course.id} courseProp={course}/>)
	})

	// Looping ang courseData database, ilalabas lahat ang laman ng course data using map()
	// We'll use this if we want to get all of the data sa database natin (CourseData)

	return (
		<Fragment>
		<h1>Courses</h1>
		{/*< CourseCard courseProp={courseData[0]} />*/} {/*If one item lang ang nasa card*/}
		{courses}
		</Fragment>
	)
}