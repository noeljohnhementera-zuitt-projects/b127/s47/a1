import { Fragment, useState, useEffect } from 'react';

import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function Login () {

	// useState() to Input Fields
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('') 

	// useState() Login Button
	const [isAuthenticated, setIsAuthenticated] = useState(false)

	// useEffect() to enable Login Button
	useEffect(() =>{
		if(email !== '' && password !== ''){
			setIsAuthenticated(true)
		}else{
			setIsAuthenticated(false)
		}
	})

	// Login alert and clear characters in the input field
	const clearInputData = (e) =>{
		e.preventDefault();
		setEmail('')
		setPassword('')

		Swal.fire({
			title: 'Authentication Successful!',
			icon: 'success',
			text: 'You have successfully logged in!'
		})
	}

	return (
		<Fragment>
			<h1>Login</h1>
			<Form onSubmit={(e)=> clearInputData(e)}>
				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
						type="email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						placeholder="Enter Your Email Address"
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type="password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						placeholder="Enter Your Password"
						required
					/>
				</Form.Group>
				{isAuthenticated ?
				<Button variant="success" type="submit">Login</Button>
								 :
				<Button variant="danger" type="submit" disabled>Login</Button>
				}
			</Form>
		</Fragment>
	)
}